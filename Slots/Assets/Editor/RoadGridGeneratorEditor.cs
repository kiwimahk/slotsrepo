﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RoadGridGenerator), true), CanEditMultipleObjects]
public class RoadGridGeneratorEditor : Editor {



	public override void OnInspectorGUI() {
		DrawDefaultInspector ();

		RoadGridGenerator generator = target as RoadGridGenerator;

		if (GUILayout.Button ("Generate Grid")) {
			generator.GenerateGrid ();
		}

	}

	void OnSceneGUI() {
	//	RoadGridGenerator gridGenerator = target as RoadGridGenerator;


		if (Event.current.type == EventType.MouseDown) {
			GameController.instance.closestSlotTo (MouseLocationAsWorldPoint ()).ToggleOccupied ();
		}

		EditorUtility.SetDirty (target);
	}

	Vector3 MouseLocationAsWorldPoint() {
		Ray r = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
		Vector3 mouseWorldPoint = r.origin;
		mouseWorldPoint.y = 0.0f;

		return mouseWorldPoint;
	}

}
