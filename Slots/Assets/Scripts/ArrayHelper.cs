﻿public static class ArrayHelper {

	public static T[] RemoveElementFromArray<T>(T element, T[] array) {
		if (!ArrayContains (element, array)) {
			return array;
		}

		if (array.Length <= 1) {
			return new T[0];
		}

		T[] result = new T[array.Length - 1];
		int j = 0;
		for (int i = 0; i < array.Length; i++) {
			if (!array[i].Equals(element)) {
				result [j++] = array [i];
			}
		}

		return result;
	}

	public static T[] RemoveIndexFromArray<T>(int index, T[] array) {
		if (index >= array.Length) {
			return array;
		}

		if (array.Length <= 1) {
			return new T[0];
		}

		T[] result = new T[array.Length - 1];
		int j = 0;
		for (int i = 0; i < array.Length; i++) {
			if (index != i) {
				result [j++] = array [i];
			}
		}

		return result;
	}

	public static T[] AddToArray<T>(T element, T[] array) {
		T[] result = new T[array.Length + 1];
		array.CopyTo (result, 0);
		result [result.Length - 1] = element;

		return result;
	}

	public static T[] AddToFrontOfArray<T>(T element, T[] array) {
		T[] result = new T[array.Length + 1];
		array.CopyTo (result, 1);
		result [0] = element;

		return result;
	}

	public static T[] AddToArrayAtIndex<T>(T element, int insertionIndex, T[] array) {
		T[] result = new T[array.Length + 1];

		for (int newIndex = 0; newIndex < result.Length; newIndex++) {
			if (newIndex < insertionIndex) {
				result [newIndex] = array [newIndex];
			} else if (newIndex == insertionIndex) {
				result [newIndex] = element;
			} else {
				result [newIndex] = array [newIndex - 1];
			}
		}

		return result;
	}

	public static bool ArrayContains<T>(T element, T[] array) {
		for (int i = 0; i < array.Length; i++) {
			if (array [i] == null) {
				array = RemoveIndexFromArray (i, array);
				return ArrayContains (element, array);
			} else if (array [i].Equals(element)) {
				return true;
			}
		}

		return false;
	}

	public static bool ArraysHaveMutualElements<T>(T[] array1, T[] array2) {
		for (int i = 0; i < array1.Length; i++) {
			for (int j = 0; j < array2.Length; j++) {
				if (array1 [i].Equals(array2 [j]))
					return true;
			}
		}
			
		return false;
	}
		

	public static T[] MutualElements<T>(T[] array1, T[] array2) {
		T[] result = new T[0];

		for (int i = 0; i < array1.Length; i++) {
			for (int j = 0; j < array2.Length; j++) {
				if (array1 [i].Equals (array2 [j]))
					result = AddToArray (array1 [i], result);
			}
		}

		return result;
	}

	public static T[] AppendArrayToArray<T>(T[] append, T[] array) {
		if (array == null)
			array = new T[0];
		
		T[] result = new T[append.Length + array.Length];
		array.CopyTo (result, 0);

		for (int i = array.Length; i < array.Length + append.Length; i++) {
			result [i] = append [i - array.Length];
		}

		return result;
	}
}
