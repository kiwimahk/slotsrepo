﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSlot : Slot {

	public DeathSlot(Vector3 position, Vector3 direction) {
		this.position = position;
		this.direction = direction;
		this.occupied = false;
	}


	public override void DrawSlotGizmo() {
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (this.position, 0.2f);
		Gizmos.DrawWireCube (this.position, new Vector3 (GameController.instance.slotLength, 1.0f, GameController.instance.slotLength));
	}
}
