﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Connection {
	SouthOutgoing,
	SouthIncoming,
	EastOutGoing,
	EastIncoming,
	NorthOutgoing,
	NorthIncoming,
	WestOutgoing,
	WestIncoming
};

public class Crossing {

	public int index;

	public static readonly Vector3[] LaneDirections = new Vector3[] { 
		Vector3.back,
		Vector3.forward,
		Vector3.right,
		Vector3.left,
		Vector3.forward, 
		Vector3.back,
		Vector3.left,
		Vector3.right
	};

	public Vector3 center;

	public Lane[] lanes;



	public Crossing(Vector3 center) {
		this.center = center;
		this.lanes = new Lane[8];

		for (int i = 0; i < this.lanes.Length; i+=2) {
			float halfSlotLength = GameController.instance.slotLength / 2f;
			Vector3 offset = Quaternion.Euler (0, 90, 0) * LaneDirections [i] * halfSlotLength;
			Vector3 position = this.center + (LaneDirections [i] * halfSlotLength * 3);

			this.lanes [i] = new Lane ();
			this.lanes [i].slots = new Slot[] { new DeathSlot (position + offset, LaneDirections [i]) };
			this.lanes [i+1] = new Lane ();
			this.lanes [i + 1].slots = new Slot[] { new DeathSlot (position - offset, LaneDirections [i + 1]) };
		}

	}

	public void CreateLanesWith (Crossing otherCrossing) {
		for (int i = 0; i < this.lanes.Length; i += 2) {
			if (Vector3.Dot (LaneDirections [i], (otherCrossing.center - this.center)) > 0) {
				this.lanes [i] = new Lane (this.lanes [i].slots [0] as DeathSlot);

				int otherIndex = (i + 4) % 8;

				this.lanes [i + 1] = new Lane (otherCrossing.lanes [otherIndex].slots [0] as DeathSlot);

				otherCrossing.lanes [otherIndex] = this.lanes [i + 1];
				otherCrossing.lanes [otherIndex + 1] = this.lanes [i];

				break;
			}
		}
	}

	public void ConnectLanes() {
		for (int i = 0; i < this.lanes.Length; i += 2) {
			int incomingIndex = i + 1;
			int turnRightIndex = (incomingIndex + 1)%8;
			int nextStraightIndex = (incomingIndex + 3) % 8;
			this.lanes [incomingIndex].ReplaceLastSlot (new CrossingSlot (	this.lanes [incomingIndex].lastSlot.position, 
																			this.lanes [incomingIndex].lastSlot.direction, 
																			this.lanes [nextStraightIndex].firstSlot, 
																			this.lanes [turnRightIndex].firstSlot ) );

		}
	}

	public int laneIndexForPoint(Vector3 point) {
		Vector3 direction = (point - this.center).normalized;

		for (int i = 0; i < 4; i++) {
			Vector3 candidateDirection = Quaternion.Euler (0, 90 * i, 0) * Vector3.back;
			for (int j = 0; j < 2; j++) {
				candidateDirection *= (j * -1);
				if (candidateDirection == direction) {
					return (i * 2) + j;   
				}
			}
		}

		return -1;
	}


	public void PlayRound() {


		for (int i = 1; i < this.lanes.Length; i += 2) {	//Cycle through our 4 incoming lanes

			for (int j = this.lanes [i].slots.Length - 1; j >= 0; j--) {		//cycle backwards through the slots in these lanes


				if (this.lanes [i].slots [j].piece != null &&				//if we find a slot with a piece in it...
					!this.lanes [i].slots [j].piece.playedThisRound) {		//that hasn't yet been played this round...


					if (this.lanes [i].slots [j] is CrossingSlot &&			//Then if we can play the piece into the next slot, we do that
					    this.lanes [i].slots [j].piece.turningFlag) {
						if (!(this.lanes [i].slots [j] as CrossingSlot).nextTurnSlot.occupied) {
							//play piece into nextTurnSlot
							PlayPieceFromSlotIntoSlot(this.lanes[i].slots[j], (this.lanes [i].slots [j] as CrossingSlot).nextTurnSlot);
						}

					} else {
						
						if (!this.lanes [i].slots [j].nextSlot.occupied) {
							//play piece into nextSlot
							PlayPieceFromSlotIntoSlot(this.lanes[i].slots[j], this.lanes [i].slots [j].nextSlot);
						}
					}

					break;		//We're only interested in playing the closest unplayed Piece to the crossing per lane
				}
			}
		}
	}

	void PlayPieceFromSlotIntoSlot(Slot fromSlot, Slot toSlot) {
		if (toSlot.occupied || !fromSlot.occupied)
			return;

		fromSlot.piece.PlayToSlot (toSlot);
		fromSlot.piece = null;
		fromSlot.occupied = false;
	}

	public void DrawCrossingGizmo() {
		Gizmos.color = new Color (0.8f, 0.6f, 0.2f);
		Gizmos.DrawWireSphere (this.center, 0.5f);
	}

}
