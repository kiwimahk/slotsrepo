﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossingSlot : Slot {

	public Slot nextTurnSlot;

	public CrossingSlot(Vector3 position, Vector3 direction, Slot nextSlot, Slot nextTurnSlot) {
		this.position = position;
		this.direction = direction;
		this.occupied = false;

		this.nextSlot = nextSlot;
		this.nextTurnSlot = nextTurnSlot;
	}


	public override void DrawSlotGizmo() {
		if (occupied) {
			Gizmos.color = Color.magenta;
		} else if (highlighted) {
			Gizmos.color = Color.yellow;
		}

		Gizmos.DrawWireSphere (this.position, 0.2f);
		Gizmos.DrawWireCube (this.position, new Vector3(GameController.instance.slotLength, 1.0f, GameController.instance.slotLength));

	}
}
