﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Slot {

	public int index;

	public Vector3 position;
	public Vector3 direction;
	public bool occupied;
	public bool highlighted;

	public Slot nextSlot;

	public Piece piece;

	public Slot() {
		this.position = Vector3.zero;
		this.direction = Vector3.forward;
		this.occupied = false;
	}

	public Slot(Vector3 position, Vector3 direction) {
		this.position = position;
		this.direction = direction;
		this.occupied = false;
	}

	public void ToggleOccupied() {
		this.occupied = !this.occupied;
	}

	public virtual void DrawSlotGizmo() {
		if (occupied) {
			Gizmos.color = Color.magenta;
		} else if (highlighted) {
			Gizmos.color = Color.yellow;
		}
	
		Gizmos.DrawWireSphere (this.position, 0.2f);
		Gizmos.DrawWireCube (this.position, new Vector3(GameController.instance.slotLength, 1.0f, GameController.instance.slotLength));

	}

}
