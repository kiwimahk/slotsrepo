﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour {

	public bool playedThisRound;
	public bool turningFlag;

	public bool transitioningToNextSlot;

	[System.NonSerialized]
	public Slot slot;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void PlayToSlot(Slot slot) {
		this.slot = slot;
		this.slot.occupied = true;
		this.slot.piece = this;

		this.playedThisRound = true;
		this.transitioningToNextSlot = true;

		this.transform.position = this.slot.position;
		this.transitioningToNextSlot = false;
	}
}
