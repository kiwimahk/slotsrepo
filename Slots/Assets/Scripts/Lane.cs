﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lane {
	public int index;


	public Slot[] slots;

	public Slot lastSlot {
		get {
			return this.slots [this.slots.Length - 1];
		}
		set {
			this.slots [this.slots.Length - 1] = value;
		}
	}

	public Slot firstSlot {
		get {
			return this.slots [0];
		}
		set {
			this.slots [0] = value;
		}
	}

	public Lane(Vector3 firstSlotPoint, Vector3 direction) {
		this.slots = new Slot[GameController.instance.slotsPerLane];
		for (int i = 0; i < this.slots.Length; i++) {
			this.slots [i] = new Slot (firstSlotPoint + (direction * GameController.instance.slotLength * i), direction);

			if (i > 0)
				this.slots [i - 1].nextSlot = this.slots [i];
		}
	}

	public Lane() {
		this.slots = new Slot[0];
	}

	public Lane(DeathSlot deathSlot) : this(deathSlot.position, deathSlot.direction) {
		
	}

	public void ReplaceLastSlot(Slot newSlot) {
		if (this.slots.Length <= 1) {
			this.slots = new Slot[] { newSlot };
		} else {
			this.lastSlot = newSlot;
			this.slots [this.slots.Length - 2].nextSlot = this.lastSlot;
		}
	}
}
