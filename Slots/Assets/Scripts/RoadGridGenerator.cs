﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGridGenerator : MonoBehaviour {

	public int gridWidth;
	public int gridHeight;


	public void GenerateGrid() {
		int numberOfCrossings = gridWidth * gridHeight;
		float slotLength = GameController.instance.slotLength;
		int slotsPerLane = GameController.instance.slotsPerLane;

		float laneLength = slotLength * slotsPerLane;
		float crossingAbstand = laneLength + (slotLength * 2.0f);

		/*Create and join crossings, creating all lanes and slots */
		GameController.instance.crossings = new Crossing[numberOfCrossings];
		for (int y = 0; y < gridHeight; y++) {
			for(int x = 0; x < gridWidth; x++) {
				Vector3 position = new Vector3 (x * crossingAbstand, 0.0f, y * crossingAbstand);
				GameController.instance.crossings [(y * gridWidth) + x] = new Crossing (position);

				if (y > 0) {
					GameController.instance.crossings [(y * gridWidth) + x].CreateLanesWith (GameController.instance.crossings [((y - 1) * gridWidth) + x]);
				}

				if (x > 0) {
					GameController.instance.crossings [(y * gridWidth) + x].CreateLanesWith (GameController.instance.crossings [(y * gridWidth) + (x - 1)]);
				}
			}
		}

		foreach (Crossing crossing in GameController.instance.crossings) {
			crossing.ConnectLanes ();
		}

		GameController.instance.lanes = new Lane[0];
		foreach (Crossing crossing in GameController.instance.crossings) {
			for (int i = 0; i < crossing.lanes.Length; i += 1) {
				GameController.instance.lanes = ArrayHelper.AddToArray (crossing.lanes[i], GameController.instance.lanes);
			}
		}
			

		GameController.instance.slots = new Slot[0];
		foreach (Lane lane in GameController.instance.lanes) {
			GameController.instance.slots = ArrayHelper.AppendArrayToArray (lane.slots, GameController.instance.slots);
		}



	}



}
